import { ViewerPage } from './app.po';

describe('viewer App', () => {
  let page: ViewerPage;

  beforeEach(() => {
    page = new ViewerPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
