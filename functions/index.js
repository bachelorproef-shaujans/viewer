const functions = require('firebase-functions');
const admin = require("firebase-admin");
admin.initializeApp(functions.config().firebase);
exports.fcmSend = functions.database.ref('/notifications/{userId}/{messageId}').onCreate(event => {
  const message = event.data.val()
  const userId  = event.params.userId
  const payload = {
    notification: {
      title: message.title,
      body: message.body,
      icon: "https://firebasestorage.googleapis.com/v0/b/sketch-inspector-6973a.appspot.com/o/static%2Flogonotificatie.png?alt=media&token=d1f511d0-7781-4e9e-8d53-b6a1bb67ee1e"
    }
  };

  admin.database()
    .ref(`/fcmTokens/${userId}`)
    .once('value')
    .then(token => token.val())
      .then(userFcmToken => {
        return admin.messaging().sendToDevice(userFcmToken, payload)
      })
      .then(res => {
        console.log("Sent Successfully", res);
      })
      .catch(err => {
        console.log(err);
      });
});
