import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AuthguardService } from './shared/authguard.service';
import { LoginComponent } from './login/login.component';
import { ProjectListComponent } from './project-list/project-list.component';
import { ProjectDetailComponent } from './project-detail/project-detail.component';
import { ViewerComponent } from './viewer/viewer.component';
import { CommentComponent } from './comment/comment.component';
import { ShortcodeComponent } from './shortcode/shortcode.component';

const appRoutes: Routes = [
  {
    path: '', component: AppComponent, children: [
      { path: 's/:id', component: ShortcodeComponent },
      { path: 'start', component: LoginComponent },
      { path: 'projects', component: ProjectListComponent, canActivate: [AuthguardService] },
      { path: 'projects/:id', component: ProjectDetailComponent, canActivate: [AuthguardService] },
      { path: 'inspect', component: ViewerComponent, canActivate: [AuthguardService] },
      { path: 'comment', component: CommentComponent, canActivate: [AuthguardService] },
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, { useHash: false })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
