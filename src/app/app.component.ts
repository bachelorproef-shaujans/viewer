import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute } from '@angular/router';
import { MessagingService } from "./shared/messaging.service";

import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  user: Observable<firebase.User>;

  constructor(
    public afAuth: AngularFireAuth,
    public router: Router,
    public route: ActivatedRoute,
    public messageService: MessagingService
  ) {
    this.user = afAuth.authState;

    this.user.subscribe((auth) => {
     if (auth) {
       if (!router.url.startsWith('/s/')) {
         router.navigateByUrl('projects');
       }
     } else {
       if (!router.url.startsWith('/s/')) {
         router.navigateByUrl('start');
       }
     }
    });
  }

  ngOnInit() {
    this.messageService.getPermission();
  }
}
