import { environment } from '../environments/environment';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { ProjectListComponent } from './project-list/project-list.component';
import { ProjectDetailComponent } from './project-detail/project-detail.component';
import { ViewerComponent } from './viewer/viewer.component';
import { ViewerLayersComponent } from './viewer-layers/viewer-layers.component';
import { ViewerPropertiesComponent } from './viewer-properties/viewer-properties.component';
import { DimensionsComponent } from './viewer-properties/dimensions/dimensions.component';
import { BordersComponent } from './viewer-properties/borders/borders.component';
import { FillsComponent } from './viewer-properties/fills/fills.component';
import { ShadowsComponent } from './viewer-properties/shadows/shadows.component';
import { TextComponent } from './viewer-properties/text/text.component';
import { ViewerHotspotsComponent } from './viewer-hotspots/viewer-hotspots.component';
import { CommentComponent } from './comment/comment.component';
import { ShortcodeComponent } from './shortcode/shortcode.component';

import { AuthService } from './shared/auth.service';
import { AuthguardService } from './shared/authguard.service';
import { ProjectService } from './shared/project.service';
import { CommentService } from './shared/comment.service';
import { MessagingService } from './shared/messaging.service';

import { ProjectColorPipe } from './shared/project-color.pipe';
import { TimeAgoPipe } from './shared/time-ago.pipe';
import { NscolorToHexPipe } from './shared/nscolor-to-hex.pipe';
import { PagecountPipe } from './shared/pagecount.pipe';
import { ReversePipe } from './shared/reverse.pipe';
import { HasKeyPipe } from './shared/has-key.pipe';
import { DecodePipe } from './shared/decode.pipe';
import { CountCommentsPipe } from './shared/count-comments.pipe';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { NgPipesModule } from 'ngx-pipes';
import { ClipboardModule } from 'ngx-clipboard';

import * as $ from 'jquery';
import * as Clipboard from 'clipboard';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ProjectListComponent,
    ProjectDetailComponent,
    ViewerComponent,
    ViewerLayersComponent,
    ViewerPropertiesComponent,
    DimensionsComponent,
    BordersComponent,
    FillsComponent,
    ShadowsComponent,
    TextComponent,
    ViewerHotspotsComponent,
    CommentComponent,
    ProjectColorPipe,
    TimeAgoPipe,
    NscolorToHexPipe,
    PagecountPipe,
    ReversePipe,
    HasKeyPipe,
    ShortcodeComponent,
    DecodePipe,
    CountCommentsPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpModule,
    NgPipesModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    ClipboardModule
  ],
  providers: [
    AuthService,
    AuthguardService,
    ProjectService,
    CommentService,
    MessagingService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
