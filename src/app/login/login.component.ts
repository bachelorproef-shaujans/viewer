import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { AuthService } from '../shared/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  message: string;

  constructor(
    public router: Router,
    public authService: AuthService
  ) { }

  loginGoogle() {
    this.authService.loginGoogle()
      .then(resolve => { console.log(resolve); this.router.navigateByUrl('projects') })
      .catch(error => { console.error(error); this.message = error.message })
  }

  loginGithub() {
    this.authService.loginGithub()
      .then(resolve => { console.log(resolve); this.router.navigateByUrl('projects') })
      .catch(error => { console.error(error); this.message = error.message })
  }

  loginAnonymous() {
    this.authService.loginAnonymous()
      .then(resolve => { console.log(resolve); this.router.navigateByUrl('projects') })
      .catch(error => { console.error(error); this.message = error.message })
  }

}
