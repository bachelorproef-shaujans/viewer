import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AuthService } from '../shared/auth.service';
import { ProjectService } from '../shared/project.service';
import { AngularFireDatabase, FirebaseObjectObservable, FirebaseListObservable } from 'angularfire2/database';
import { Project } from '../shared/project.model';

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.scss']
})
export class ProjectDetailComponent implements OnInit {

  project: Project;
  selectedPage;
  hosturl;
  sharesuccess = false;

  constructor(
    public router: Router,
    public route: ActivatedRoute,
    public authService: AuthService,
    public projectService: ProjectService
  ) {
    if (!projectService.guestmode) {
      route.params.forEach(params => {
        projectService.getProject(params['id'], false).subscribe((content) => {
          this.project = content;

          this.selectAllPages();
        })
      });
    } else {
      this.project = projectService.selectedProject;
      this.selectAllPages();
    }
  }

  ngOnInit() {
    this.hosturl = window.location.host + '/s/';
    $('html').removeClass('overflow');
    $('body').removeClass('overflow');
  }

  /**
   * Select all pages and build array with all artboards
   */
  selectAllPages() {
    let pages = this.project.pages;
    let artboards = { artboards: [] };

    /**
     * Remove all active classes
     */
    this.removeActiveClasses();

    /**
     * Add active class
     */
    if (document.getElementById('selectallpages')) { document.getElementById('selectallpages').classList.add('active'); }

    /**
     * Build array with all artboards
     */
    for (let page in pages.slice().reverse()) {
      for (let artboard in pages[page].artboards) {
        artboards.artboards.push(pages[page].artboards[artboard]);
      }
    }

    this.selectedPage = artboards;
  }

  saveProjectName() {
    this.projectService.updateProjectName(this.project).then(() => { });
  }

  deleteProject() {
    this.projectService.deleteProject(this.project).then(() => { this.router.navigateByUrl('/projects'); });
  }

  /**
   * Select page
   * @param page
   */
  showPage(page) {
    /**
     * Select active page
     */
    this.selectedPage = page;

    /**
     * Remove all active classes
     */
    this.removeActiveClasses();

    /**
     * Add active class
     */
    document.getElementById(page.pageId).classList.add('active');
  }

  /**
   * Remove 'active' class from all pages
   */
  removeActiveClasses() {
    let divpages = document.querySelectorAll('.page');
    [].forEach.call(divpages, function(div) {
      div.classList.remove('active');
    });
  }

  showInspector(selectedArtboard) {
    /**
     * Reset viewer
     */
    this.projectService.selectedLayer = null;

    this.projectService.selectedArtboardId = selectedArtboard.id;
    this.router.navigateByUrl('inspect');
  }
}
