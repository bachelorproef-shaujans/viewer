import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Router } from '@angular/router';
import { AuthService } from '../shared/auth.service';
import { ProjectService } from '../shared/project.service';
import { Project } from '../shared/project.model';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss']
})
export class ProjectListComponent implements OnInit {

  projects;

  sketchPluginUrl;

  constructor(
    public router: Router,
    public authService: AuthService,
    public projectService: ProjectService
  ) { }

  ngOnInit() {
    this.projectService.getProjects().subscribe((projects) => {
      this.projects = projects;
      if (this.projects.length === 0) {
        this.authService.authUser().subscribe((user) => {
          if (user.isAnonymous) {
            this.authService.logout().then(() => {});
          }
        });
      }
    });

    this.sketchPluginUrl = environment.sketchplugin;

    $('html').removeClass('overflow');
    $('body').removeClass('overflow');
  }

  showProject(project) {
    this.closeSketchModal();
    this.projectService.selectedProject = project;
    this.router.navigateByUrl('projects/' + project.$key);
  }

  logout() {
    this.authService.logout().then((result) => {
      console.log(result);
    });
  }

  niceWord(i) {
    let items = ['wonderfull', 'beautifull', 'pretty', 'gorgeous'];

    if (i >= items.length) {
      i = i % items.length;
    }

    return items[i];
  }

  closeSketchModal() {
    this.projectService.showSketchModal = false;
  }

  openSketchModal() {
    this.projectService.showSketchModal = true;
  }
}
