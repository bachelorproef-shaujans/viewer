import { Pipe, PipeTransform } from '@angular/core';
import { ProjectService } from './project.service';

@Pipe({
  name: 'countComments'
})
export class CountCommentsPipe implements PipeTransform {

  constructor(public projectService: ProjectService) { }

  transform(value: any, args?: any): any {
    let counter = 0;

    if (value) {
      for (let artboardComments of value) {
        if (artboardComments.$key === args) {
          console.log('match!');
          counter = Object.keys(artboardComments).length;
        }
      }
    }

    if (counter === 0) {
      return 'No feedback yet';
    } else {
      return counter + ' comments';
    }
  }

}
