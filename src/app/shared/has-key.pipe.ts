import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'hasKey'
})
export class HasKeyPipe implements PipeTransform {

  transform(value: any, args: any): any {
    return args.split('.').every(function(x) {
      if (typeof value != 'object' || value === null)
        return false;
      value = value[x];
      return true;
    });
  }

}
