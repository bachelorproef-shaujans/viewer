import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'NSColorToHex'
})
export class NscolorToHexPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const hexValue = [];
    this.componentArray(hexValue, value.red, value.green, value.blue);
    return `#${this.hex(hexValue[0])}${this.hex(hexValue[1])}${this.hex(hexValue[2])}`;
  }

  msToRGB(value) {
    return (Math.round(value * 255));
  }

  componentArray(hexValue, r, g, b) {
    hexValue.push(this.msToRGB(r));
    hexValue.push(this.msToRGB(g));
    hexValue.push(this.msToRGB(b));
    return hexValue;
  }

  hex(x) {
    return (`0${parseInt(x).toString(16)}`).slice(-2);
  }

}
