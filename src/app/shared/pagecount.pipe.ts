import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pagecount'
})
export class PagecountPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let pageCount = 0;
    let artboardCount = 0;
    let pageWord;
    let artboardWord;

    /**
     * Count pages
     */
    for (let page in value) {
      pageCount++;

      for (let artboard in value[page].artboards) {
        artboardCount++;
      }
    }

    (pageCount === 1) ? pageWord = ' page' : pageWord = ' pages';
    (artboardCount === 1) ? artboardWord = ' artboard' : artboardWord = ' artboards';

    return pageCount + pageWord + ' and ' + artboardCount +  artboardWord;
  }

}
