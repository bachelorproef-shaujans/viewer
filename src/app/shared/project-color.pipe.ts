import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'projectcolor'
})
export class ProjectColorPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return '#' + this.intToRGB(this.hashCode(value));
  }

  hashCode(str) {
    let hash = 0;
    for (let i = 0; i < str.length; i++) {
      hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    return hash;
  }

  intToRGB(i){
    const c = (i & 0x00FFFFFF)
      .toString(16)
      .toUpperCase();

    return '0000'.substring(0, 6 - c.length) + c;
  }

}
