export interface Project {
  $key?: string;
  $ref?: string;
  name: string;
  pages: any;
  files: any;
  shortcode: any;
  created: number;
  updated: number;
}
