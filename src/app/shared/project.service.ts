import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { environment } from '../../environments/environment';
import { AuthService } from './auth.service';
import { Project } from './project.model';

import { FirebaseApp } from 'angularfire2';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, FirebaseObjectObservable, FirebaseListObservable } from 'angularfire2/database';
import 'firebase/storage';
import * as firebase from 'firebase/app';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import * as JSZip from 'jszip';

@Injectable()
export class ProjectService {

  public selectedProject: Project;
  public selectedPage;
  public selectedArtboardId;
  public selectedLayer;
  public projectComments;

  public user: firebase.User;
  public uid: string;
  public firebasestorage: any;

  public guestmode = false;
  public authorUid;
  public showSketchModal = true;
  public loading = false;
  public layerCss = null;

  constructor(
    public afAuth: AngularFireAuth,
    public afDb: AngularFireDatabase,
    public fbApp: FirebaseApp,
    public authService: AuthService,
    public router: Router,
    public http: Http
  ) {
    this.afAuth.authState.subscribe(auth => {
      if (auth !== undefined && auth !== null) {
        this.uid = auth.uid;
        this.user = auth;
      }
    });

    this.firebasestorage = fbApp.storage();
  }

  initializeProject(): Project {
    return {
      name: '',
      files: { original: '', sketch: '' },
      pages: [],
      shortcode: '',
      created: Date.now(),
      updated: Date.now()
    };
  }

  newProject(event) {
    let file = event.target.files[0];
    let jszip = new JSZip;
    let sketchZip = new JSZip;
    let sketchFilename;
    let sketchFile;
    let self = this;

    /**
     * If filesize is greater then 30MB
     */
    if (file.size > environment.maxFilesize) {
      console.error('File is to big, max 30MB');
      return false;
    }

    /**
     * Check file extension
     * Must be .sketchinspect
     */
    if (file.name.split('.').pop() !== 'sketchinspect') {
      this.showSketchModal = true;
      console.error('This is not a Sketchinspect file. Download our plugin.');
      return false;
    }

    /**
     * Check if user is logged in
     */
    if (!this.uid) {
      console.error('User must be logged in');
      return false;
    }

    /**
     * Show loading
     */
    this.loading = true;

    /**
     * Hide Sketch plugin modal
     */
    this.showSketchModal = false;

    /**
     * Initialize new project
     */
    let project: Project = this.initializeProject();

    /**
     * Create key for project
     */
    let key = this.afDb.list('projects/' + this.uid).$ref.ref.push().key;

    /**
     * Get filename for project name
     */
    project.name = file.name.substr(0, file.name.lastIndexOf('.'));
    project.name = project.name.charAt(0).toUpperCase() + project.name.slice(1);

    /**
     * Generate shortcode
     */
    project.shortcode = this.generateShortcode(key);

    /**
     * Load Sketchinspect file
     */
    jszip.loadAsync(file).then((zip) => {
      zip.files['data.js'].async('string')
        .then((content) => {
          let datajs = JSON.parse(content);
          sketchFilename = datajs.sketchName;
          for (let page in datajs.pageData) {
            project.pages.push(datajs.pageData[page]);
          }
          return zip;
        })
        .then((zip2) => {

          /**
           * Open sketch file
           */
          zip2.files[sketchFilename].async('blob')
            .then((content) => {
              sketchZip.loadAsync(content).then((sketchfile) => {
                sketchFile = sketchfile;
              })
            .then(() => {
              /**
               * Loop every page
               */
              for (let page in project.pages) {
                /**
                 * Create pagedata artboards array
                 */
                project.pages[page].artboards = [];

                /**
                 * Save Sketch page
                 */
                sketchFile.files['pages/' + project.pages[page].pageId + '.json'].async('blob')
                  .then((sketchpage) => {
                    self.firebasestorage.ref('projects/' + self.uid + '/' + key + '/pages/' + project.pages[page].pageId + '.json')
                      .put(sketchpage)
                      .then((snapshot) => {
                        // project.files.pages[project.pages[page].pageId] = { id: project.pages[page].pageId, url: snapshot.downloadURL};
                        project.pages[page]['data'] = snapshot.downloadURL;
                      });
                  });

                /**
                 * Open Sketch page
                 */
                sketchFile.files['pages/' + project.pages[page].pageId + '.json'].async('string')
                  .then((sketchpage) => {
                    // console.log(sketchpage);

                    /**
                     * Loop evert artboard in page
                     */
                    for (let artboard in project.pages[page].artboardId) {
                      let artboardId = project.pages[page].artboardId[artboard];

                      /**
                       * Save files to artboard
                       */
                      zip2.files[artboardId + '/artboard.png'].async('blob')
                        .then((image) => {
                          self.firebasestorage.ref('projects/' + self.uid + '/' + key + '/artboards/' + artboardId + '.png')
                            .put(image)
                            .then((snapshot) => {
                              let artboardName = this.getArtboardName(sketchpage, artboardId);
                              project.pages[page].artboards[artboard] = { id: artboardId, name: artboardName, url: snapshot.downloadURL};
                            });
                        });
                    }

                    /**
                     * Remove artboardId
                     */
                    delete project.pages[page].artboardId;
                  });
              }
            });
          });
        });
    });


    /**
     * Safe original file
     */
    return this.firebasestorage.ref('projects/' + this.uid + '/' + key + '/' + file.name)
      .put(file)
      .then((snapshot) => {
        project.files.original = snapshot.downloadURL;

        /**
         * Save project in database
         */
        this.afDb.object('projects/' + this.uid + '/' + key).set(project);

        /**
         * Set project to new project
         * @type {Project}
         */
        this.selectedProject = project;

        /**
         * Hide loading
         */
        this.loading = false;
      })
  }

  updateProjectName(project: Project) {
    if (this.uid !== undefined && this.uid !== null) {
      return this.afDb.object('projects/' + this.uid + '/' + project.$key).update({
        name: project.name,
        updated: Date.now()
      });
    }
  }

  deleteProject(project: Project) {
    return this.afDb.object('projects/' + this.uid + '/' + project.$key).remove();
  }

  getArtboardName(layers, id) {
    let page = JSON.parse(layers);

    for (let artboard in page.layers) {
      if (page.layers[artboard].do_objectID === id) {
        return page.layers[artboard].name;
      }
    }
  }

  getProjects(): Observable<Project[]> {
    return this.afDb.list('projects/' + this.uid);
  }

  getProject(projectId, userId): FirebaseObjectObservable<any> {
    let uid = this.uid;
    if (userId) { uid = userId }

    this.getCommentsForProject(projectId);

    return this.afDb.object('projects/' + uid + '/' + projectId);
  }

  selectArtboard(selectedArtboard) {
    this.selectedArtboardId = selectedArtboard;

    for (let page in this.selectedProject.pages) {
      for (let artboard in this.selectedProject.pages[page].artboards) {
        if (this.selectedProject.pages[page].artboards[artboard].id === selectedArtboard) {
          this.selectedPage = this.selectedProject.pages[page];
        }
      }
    }

    return this.selectedPage;
  }

  generateCss(layer) {
    let unit = 'px';
    let css = [];

    /**
     * Generate dimensions
     */
    if (layer.frame) {
      css.push({ 'label': 'width', 'value': layer.frame.width + unit });
      css.push({ 'label': 'height', 'value': layer.frame.height + unit });
    }


    if (layer.style) {
      /**
       * Fills
       */
      if (layer.style.fills) {
        for (let fill of layer.style.fills) {
          if (fill.isEnabled) {
            /**
             * Generate background color
             */
            if (fill.fillType === 0) {
              const hexValue = [];
              this.componentArray(hexValue, fill.color.red, fill.color.green, fill.color.blue);
              let color = `#${this.hex(hexValue[0])}${this.hex(hexValue[1])}${this.hex(hexValue[2])}`;
              css.push({ 'label': 'background-color', 'value': color });
            }

            /**
             * Generate background gradient
             */
            if (fill.fillType === 1) {
              let stops = '';

              for (let stop of fill.gradient.stops) {
                const hexValue = [];
                this.componentArray(hexValue, stop.color.red, stop.color.green, stop.color.blue);
                let color = `#${this.hex(hexValue[0])}${this.hex(hexValue[1])}${this.hex(hexValue[2])}`;
                stops += color + ' ' + (stop.position * 100) + '%,';
              }
              css.push({ 'label': 'background', 'value': 'linear-gradient(' + stops.slice(0, -1) + ')'});
            }
          }
        }

        /**
         * Borders
         */
        if (layer.style.borders) {
          for (let border of layer.style.borders) {
            if (border.isEnabled) {
              const hexValue = [];
              this.componentArray(hexValue, border.color.red, border.color.green, border.color.blue);
              let color = `#${this.hex(hexValue[0])}${this.hex(hexValue[1])}${this.hex(hexValue[2])}`;
              css.push({ 'label': 'border', 'value': border.thickness + unit + ' solid ' + color });
            }
          }
        }

        /**
         * Shadows
         */
        if (layer.style.shadows) {
          for (let shadow of layer.style.shadows) {
            if (shadow.isEnabled) {
              const hexValue = [];
              this.componentArray(hexValue, shadow.color.red, shadow.color.green, shadow.color.blue);
              let color = `#${this.hex(hexValue[0])}${this.hex(hexValue[1])}${this.hex(hexValue[2])}`;
              css.push({ 'label': 'box-shadow', value: shadow.offsetX + 'px ' + shadow.offsetY + 'px ' + shadow.blurRadius + 'px ' + shadow.spread + 'px ' + color});
            }
          }
        }

        /**
         * Inner Shadows
         */
        if (layer.style.innerShadows) {
          for (let shadow of layer.style.innerShadows) {
            if (shadow.isEnabled) {
              const hexValue = [];
              this.componentArray(hexValue, shadow.color.red, shadow.color.green, shadow.color.blue);
              let color = `#${this.hex(hexValue[0])}${this.hex(hexValue[1])}${this.hex(hexValue[2])}`;
              css.push({ 'label': 'box-shadow', value: shadow.offsetX + 'px ' + shadow.offsetY + 'px ' + shadow.blurRadius + 'px ' + shadow.spread + 'px ' + color + ' inset'});
            }
          }
        }
      }
    }

    this.layerCss = css;
  }

  /**
   * Convert NSColor
   */
  msToRGB(value) {
    return (Math.round(value * 255));
  }
  componentArray(hexValue, r, g, b) {
    hexValue.push(this.msToRGB(r));
    hexValue.push(this.msToRGB(g));
    hexValue.push(this.msToRGB(b));
    return hexValue;
  }
  hex(x) {
    return (`0${parseInt(x).toString(16)}`).slice(-2);
  }


  /**
   *
   * @param shortcode
   * @returns {FirebaseObjectObservable<any>}
   */
  getShorcode(shortcode) {
    return this.afDb.object('shortcodes/' + shortcode);
  }

  /**
   *
   * @param projectId
   * @returns {string}
   */
  generateShortcode(projectId) {
    let shortcode = { userId: this.uid, projectId: projectId };
    let randomId = this.generateUniqueId();
    let key = this.afDb.list('shortcodes/').$ref.ref.push().key;
    this.afDb.object('shortcodes/' + key).set(shortcode);
    return key;
  }

  /**
   *
   * @returns {string}
   */
  generateUniqueId() {
    let timestamp = + new Date();
    let ts = timestamp.toString();
    let parts = ts.split('').reverse();
    let id = '';

    for (let i = 0; i < 8; i++) {
      let index = Math.floor(Math.random() * ( 0 - parts.length + 1 )) + parts.length;
      id += parts[index];
    }

    return id;
  }

  /**
   *
   * @param artboardId
   * @returns {FirebaseListObservable<any[]>}
   */
  getComments(artboardId) {
    let projectId = this.selectedProject.$key;
    return this.afDb.list('comments/' + projectId + '/' + artboardId);
  }


  /**
   * Get comments count
   * @param artboardId
   */
  getCommentsForProject(projectId) {
    this.afDb.list('comments/' + projectId ).subscribe((comments) => {
      console.log(comments);
      this.projectComments = comments;
    });
  }

  /**
   *
   * @param comment
   */
  postComment(comment) {
    let projectId = this.selectedProject.$key;
    let artboardId = this.selectedArtboardId;
    let key = this.afDb.list('comments/' + projectId + '/' + artboardId).$ref.ref.push().key;
    comment.user = { uid: this.user.uid, name: this.user.displayName, avatar: this.user.photoURL };
    comment.created = Date.now();
    this.afDb.object('comments/' + projectId + '/' + artboardId + '/' + key).set(comment);

    if (this.authorUid) {
      console.log('create notification');
      let notification = { 'title': 'New comment on Sketch inspector', body: 'Take a look at your project: ' + this.selectedProject.name };
      let messageKey = this.afDb.list('notifications/' + this.authorUid).$ref.ref.push().key;
      this.afDb.object('notifications/' + this.authorUid + '/' + messageKey).set(notification);
    }
  }
}
