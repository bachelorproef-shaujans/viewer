import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AuthService } from '../shared/auth.service';
import { ProjectService } from '../shared/project.service';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-shortcode',
  templateUrl: './shortcode.component.html',
  styleUrls: ['./shortcode.component.scss']
})
export class ShortcodeComponent implements OnInit {

  project;
  projectId;
  message;

  constructor(
    public router: Router,
    public route: ActivatedRoute,
    public authService: AuthService,
    public projectService: ProjectService,
    public afAuth: AngularFireAuth
  ) {
    this.projectService.guestmode = true;

    route.params.forEach(params => {
      this.projectId = params['id'];

      /**
       * If already logged in
       */
      afAuth.authState.subscribe((auth) => {
        if (auth) {
          this.getProject();
        }
      });
    });
  }

  ngOnInit() {
  }

  getProject() {
    this.projectService.getShorcode(this.projectId).subscribe((projectDetails) => {
      this.projectService.getProject(projectDetails.projectId, projectDetails.userId).subscribe((project) => {
        this.projectService.selectedProject = project;
        this.projectService.authorUid = projectDetails.userId;
        this.router.navigateByUrl('projects/' + project.$key);
      });
    });
  }

  loginGoogle() {
    this.authService.loginGoogle()
      .then(resolve => { console.log(resolve); this.getProject() })
      .catch(error => { console.error(error); this.message = error.message })
  }

  loginGithub() {
    this.authService.loginGithub()
      .then(resolve => { console.log(resolve); this.getProject() })
      .catch(error => { console.error(error); this.message = error.message })
  }

}
