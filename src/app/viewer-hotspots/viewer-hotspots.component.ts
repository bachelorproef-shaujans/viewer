import { Component, OnInit, Input, Renderer } from '@angular/core';
import { ProjectService } from '../shared/project.service';

@Component({
  selector: 'app-viewer-hotspots',
  templateUrl: './viewer-hotspots.component.html',
  styleUrls: ['./viewer-hotspots.component.scss']
})
export class ViewerHotspotsComponent implements OnInit {

  @Input() layers;

  constructor(
    public projectService: ProjectService
  ) { }

  ngOnInit() {
  }

  getStyle(layer, idx) {
    let styles;

    if (layer['_class'] === 'page') {
      styles = {
        'overflow': 'visible',
        'width': '99999px'
      }
    } else if (layer['_class'] === 'artboard') {
      styles = {
        'position': 'relative',
        'overflow': 'hidden',
        'float': 'none',
        'margin': '50px auto',
        'width': layer.frame.width + 'px',
        'height': layer.frame.height + 'px',
      }
    } else {
      styles = {
        'position': 'absolute',
        'overflow': 'hidden',
        'width': layer.frame.width + 'px',
        'height': layer.frame.height + 'px',
        'top': layer.frame.y + 'px',
        'left': layer.frame.x + 'px',
        'z-index': 1000 + idx
      }
    }

    return styles;
  }

  layerInformation(layer) {
    let classname = layer['_class'];
    let visible = true;

    if (layer['_class'] === 'shapeGroup') {
      classname = layer.layers[0]['_class'];
    }

    const hiddenElements = ['rectangle', 'oval', 'star', 'triangle', 'shapePath'];

    if (hiddenElements.indexOf(layer['_class']) > -1) {
      visible = false;
    }

    return {'classname': classname, 'visible': visible};
  }

  getClassname(layer) {
    return this.layerInformation(layer).classname;
  }

  isVisible(layer) {
    return this.layerInformation(layer).visible;
  }

  highlightHotspot(event, layer, highlight, select) {
    event.stopPropagation();

    let objectID = layer['do_objectID'];
    let hotspot = document.querySelector('.design [data-objectid=\'' + objectID + '\']');

    if (highlight) {
      this.removeClasses('.design .hotspot', 'highlight');
      hotspot.classList.add('highlight');
    } else {
      if (event) {
        if (!event.target.parentElement.classList.contains('selected')) {
          hotspot.classList.remove('highlight');
        }
      }
    }

    if (select) {
      this.removeClasses('.design .hotspot', 'selected');
      this.removeClasses('.design .hotspot', 'highlight');
      hotspot.classList.add('selected');

      /**
       * Select layer
       */
      this.removeClasses('.layers li', 'selected');
      let layerItem = document.querySelector('.layers [data-objectid=\'' + objectID + '\']');

      if (layer._class !== 'group') {
        layerItem.classList.add('selected');
      } else {
        layerItem.classList.add('open');
      }

      /**
       * Open parents
       */
      $(layerItem).parents('.group').addClass('open');

      /**
       * Select layer
       */
      this.projectService.selectedLayer = layer;

      /**
       * Generate css
       */
      this.projectService.generateCss(layer);
    }
  }

  removeClasses(selector, classname) {
    let divpages = document.querySelectorAll(selector);
    [].forEach.call(divpages, function(div) {
      div.classList.remove(classname);
    });
  }
}

