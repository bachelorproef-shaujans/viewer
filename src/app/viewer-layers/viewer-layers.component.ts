import { Component, OnInit, Input, Renderer } from '@angular/core';
import { ProjectService } from '../shared/project.service';

@Component({
  selector: 'app-viewer-layers',
  templateUrl: './viewer-layers.component.html',
  styleUrls: ['./viewer-layers.component.scss']
})
export class ViewerLayersComponent implements OnInit {

  @Input() layers;

  constructor(
    public projectService: ProjectService
  ) { }

  ngOnInit() {
  }

  selectLayer(event, layer) {
    event.target.parentElement.classList.toggle('open');
    console.log(layer);

    if (layer['_class'] !== 'group') {
      /**
       * Remove all selected classes
       */
      this.removeClasses('.sidebar__box.layers li', 'selected');

      /**
       * Toggle selected class to selected layer
       */
      event.target.parentElement.classList.toggle('selected');

      /**
       * Add selected layer to service
       */
      this.projectService.selectedLayer = layer;

      /**
       * Generate css
       */
      this.projectService.generateCss(layer);

      /**
       * Highlight hotspot
       */
      this.highlightHotspot(false, layer, true, true);
    }
  }

  highlightHotspot(event, layer, highlight, select) {
    let objectID = layer['do_objectID'];
    let hotspot = document.querySelector('.design [data-objectid=\'' + objectID + '\']');

    if (highlight) {
      this.removeClasses('.design .hotspot', 'highlight');
      hotspot.classList.add('highlight');
    } else {
      if (event) {
        if (!event.target.parentElement.classList.contains('selected')) {
          hotspot.classList.remove('highlight');
        }
      }
    }

    if (select) {
      this.removeClasses('.design .hotspot', 'selected');
      this.removeClasses('.design .hotspot', 'highlight');
      hotspot.classList.add('selected');
    }
  }

  layerInformation(layer) {
    let classname: string = layer['_class'];
    let visible: boolean = true;

    if (layer['_class'] === 'shapeGroup') {
      classname = layer.layers[0]['_class'];
    }

    const hiddenElements = ['rectangle', 'oval', 'star', 'triangle', 'shapePath'];

    if (hiddenElements.indexOf(layer['_class']) > -1) {
      visible = false;
    }

    return {'classname': classname, 'visible': visible};
  }

  getClassname(layer) {
    return this.layerInformation(layer).classname;
  }

  isVisible(layer) {
    return this.layerInformation(layer).visible;
  }

  removeClasses(selector, classname) {
    let divpages = document.querySelectorAll(selector);
    [].forEach.call(divpages, function(div) {
      div.classList.remove(classname);
    });
  }

}
