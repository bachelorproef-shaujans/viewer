import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-borders',
  templateUrl: './borders.component.html',
  styleUrls: ['./borders.component.scss']
})
export class BordersComponent implements OnInit {

  @Input() layer;

  constructor() { }

  ngOnInit() {
  }

  getBackgroundColorStyle(hexColor) {
    return {'background-color': hexColor};
  }

  hasEnabledBorders(layer) {
    if (!layer.style.hasOwnProperty('borders')) return false;

    for (let border of layer.style.borders) {
      if (border.isEnabled === true) {
        return true;
      }
    }

    return false;
  }
}
