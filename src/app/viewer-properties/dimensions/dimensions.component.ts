import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-dimensions',
  templateUrl: './dimensions.component.html',
  styleUrls: ['./dimensions.component.scss']
})
export class DimensionsComponent implements OnInit {
  @Input() layer;

  constructor() { }

  ngOnInit() {
  }

}
