import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-fills',
  templateUrl: './fills.component.html',
  styleUrls: ['./fills.component.scss']
})
export class FillsComponent implements OnInit {

  @Input() layer;

  constructor() { }

  ngOnInit() {
  }

  getBackgroundColorStyle(type, hexColor) {
    if (type === 'solid') {
      return {'background-color': hexColor};
    }
  }

  hasEnabledFills(layer) {
    if (!layer.style.hasOwnProperty('fills')) return false;

    for (let fill of layer.style.fills) {
      if (fill.isEnabled === true) {
        return true;
      }
    }

    return false;
  }

}
