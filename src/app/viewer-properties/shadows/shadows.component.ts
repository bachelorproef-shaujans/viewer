import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-shadows',
  templateUrl: './shadows.component.html',
  styleUrls: ['./shadows.component.scss']
})
export class ShadowsComponent implements OnInit {

  @Input() layer;

  constructor() { }

  ngOnInit() {
  }

  getBackgroundColorStyle(hexColor) {
    return {'background-color': hexColor};
  }

  hasEnabledShadows(layer) {
    if (!layer.style.hasOwnProperty('shadows')) return false;

    for (let shadow of layer.style.shadows) {
      if (shadow.isEnabled === true) {
        return true;
      }
    }

    return false;
  }

  hasEnabledInnerShadows(layer) {
    if (!layer.style.hasOwnProperty('innerShadows')) return false;

    for (let shadow of layer.style.innerShadows) {
      if (shadow.isEnabled === true) {
        return true;
      }
    }

    return false;
  }
}
