import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-viewer-properties',
  templateUrl: './viewer-properties.component.html',
  styleUrls: ['./viewer-properties.component.scss']
})
export class ViewerPropertiesComponent implements OnInit {
  @Input() layer;

  constructor() { }

  ngOnInit() {
  }

}
