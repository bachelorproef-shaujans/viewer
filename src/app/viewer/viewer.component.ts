import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../shared/project.service';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

@Component({
  selector: 'app-viewer',
  templateUrl: './viewer.component.html',
  styleUrls: ['./viewer.component.scss']
})
export class ViewerComponent implements OnInit {

  selectedPage;
  selectedArtboardId;
  selectedArtboardInfo;
  selectedArtboardData;
  modeInspect = true;
  modeComment = false;

  comments;

  newCommentForm = false;
  newCommentHotspotPosition;
  newCommentHotspot;
  newCommentAnswer = null;
  newCommentContent;
  newCommentMessage;

  constructor(
    public projectService: ProjectService,
    public http: Http
  ) { }

  ngOnInit() {
    this.selectedArtboardId = this.projectService.selectedArtboardId;
    this.selectedPage = this.projectService.selectArtboard(this.selectedArtboardId);
    this.selectedArtboardInfo = this.getArtboardInfo(this.selectedArtboardId);

    this.http.get(this.selectedPage.data).subscribe((content) => {
      this.selectedArtboardData = this.getArtboardData(JSON.parse(content['_body']), this.projectService.selectedArtboardId);
    });

    $('.interface__viewer').scrollLeft(6400);
    $('.interface__viewer').scrollTop(6400);

    $('html').addClass('overflow');
    $('body').addClass('overflow');
  }

  /**
   * Get artboard info
   * @param id
   * @returns {any}
   */
  getArtboardInfo(id) {
    for (let artboard in this.selectedPage.artboards) {
      if (this.selectedPage.artboards[artboard].id === id) {
        return this.selectedPage.artboards[artboard];
      }
    }
  }

  /**
   * Get artboard data
   * @param id
   * @returns {any}
   */
  getArtboardData(pagedata, id) {
    for (let artboard in pagedata.layers) {
      if (pagedata.layers[artboard].do_objectID === id) {
        return this.selectedArtboardData = pagedata.layers[artboard];
      }
    }
  }

  /**
   * Get Artboard styling
   * @param artboard
   * @returns {{width: string, height: string, margin-left: string, margin-top: string}}
   */
  getArtboardStyle(artboard) {
    return {
      'width': artboard.frame.width + 'px',
      'height': artboard.frame.height + 'px',
      'margin-left': 'calc(50vw - ' + artboard.frame.width * .5 + 'px)',
      'margin-top': 'calc(50vh - ' + artboard.frame.height * .5  + 'px)'
    };
  }

  /**
   * Unselect layer
   */
  unselectLayer() {
    this.projectService.selectedLayer = null;
    this.projectService.layerCss = null;
    this.removeClasses('.layers li', 'open');
    this.removeClasses('.layers li', 'selected');
    this.removeClasses('.design .hotspot', 'highlight');
    this.removeClasses('.design .hotspot', 'selected');
  }

  /**
   * Open all layergroups
   */
  openAllLayer() {
    let divpages = document.querySelectorAll('.layers li');
    [].forEach.call(divpages, function(div) {
      div.classList.add('open');
    });
  }

  /**
   * Close all layergroups
   */
  closeAllLayer() {
    this.removeClasses('.layers li', 'open');
  }

  /**
   * Remove class
   * @param selector
   * @param classname
   */
  removeClasses(selector, classname) {
    let divpages = document.querySelectorAll(selector);
    [].forEach.call(divpages, function(div) {
      div.classList.remove(classname);
    });
  }

  /**
   * Enable comment mode
   */
  enableComments() {
    this.modeInspect = false;
    this.modeComment = true;
    this.getComments();
    this.unselectLayer();
  }

  /**
   * Enable inspect mode
   */
  enableInspector() {
    this.modeInspect = true;
    this.modeComment = false;
    this.unselectLayer();
  }

  /**
   *
   */
  getComments() {
    this.projectService.getComments(this.selectedArtboardId).subscribe((comments) => {
      this.comments = comments;
    });
  }

  /**
   *
   * @param comment
   * @returns {{display: string, top: any, left: any}}
   */
  getCommentStyle(comment) {
    return {
      'display': 'block',
      'top': comment.y,
      'left': comment.x
    }
  }

  /**
   *
   * @param event
   */
  viewThread(event) {
    event.stopPropagation();
  }

  /**
   *
   * @param comment
   */
  highlightComment(comment) {
    if (!comment.answerId) {
      document.querySelector('.commentHotspot#' + comment.$key).classList.toggle('highlight');
      document.querySelector('.commentlist li#' + comment.$key).classList.toggle('highlight');
    }
  }

  /**
   *
   * @param layer
   * @param axis
   * @returns {any}
   */
  getOutlineStyles(layer, axis) {
    let selectedElement = $('.hotspot[data-objectid=\'' + layer.do_objectID + '\']');
    let styles = {};

    let top = selectedElement.offset().top -  $('.design').offset().top;
    let left = selectedElement.offset().left -  $('.design').offset().left;
    let width = layer.frame.width;
    let height = layer.frame.height;

    if (axis === 'x') {
      styles = { 'display': 'block', 'top': top + 'px', 'height': height + 'px' };
    } else if (axis === 'y') {
      styles = { 'display': 'block', 'left': left + 'px', 'width': width + 'px' };
    } else if (axis === 'mx') {
      styles = { 'display': 'block', 'top': top + 'px', 'left': left + 'px', 'width': width + 'px' }
    } else if (axis === 'mxvalue') {
      return width;
    } else if (axis === 'my') {
      styles = { 'display': 'block', 'top': top + 'px', 'left': left + 'px', 'height': height + 'px', 'line-height': height + 'px' }
    } else if (axis === 'myvalue') {
      return height;
    }

    return styles;
  }

  /**
   *
   * @param event
   */
  newComment(event) {
    this.newCommentForm = true;
    this.newCommentAnswer = null;

    this.newCommentHotspotPosition = {
      left: Math.round((event.pageX - $('.design').offset().left)),
      top: Math.round((event.pageY - $('.design').offset().top))
    };

    this.newCommentHotspot = {
      'display': 'block',
      'top': this.newCommentHotspotPosition.top - 36 + 'px',
      'left': this.newCommentHotspotPosition.left - 36 + 'px',
    };
  }

  /**
   *
   * @param comment
   */
  replyComment(comment) {
    console.log(comment);
    this.newCommentAnswer = comment;
    this.newCommentForm = true;
  }

  /**
   * Post comment
   */
  postComment() {
    if (this.newCommentContent.length < 8) {
      this.newCommentMessage = 'Your feedback is to short';
      return false;
    }

    let answerId = null;
    if (this.newCommentAnswer) {
      answerId = this.newCommentAnswer.$key;
    }

    let hotspotX = 0;
    let hotspotY = 0;
    if (this.newCommentHotspot) {
      hotspotX = this.newCommentHotspot.left;
      hotspotY = this.newCommentHotspot.top;
    }

    let comment = {
      answerId: answerId,
      x: hotspotX,
      y: hotspotY,
      content: this.newCommentContent
    };

    this.projectService.postComment(comment);

    this.newCommentForm = false;
    this.newCommentContent = '';
    this.newCommentAnswer = null;
    this.newCommentHotspotPosition = { top: 0, left: 0 };
    this.newCommentHotspot = { display: 'none', top: 0, left: 0 };
  }

  /**
   *
   */
  closeForm() {
    this.newCommentForm = false;
    this.newCommentContent = '';
    this.newCommentAnswer = null;
    this.newCommentHotspotPosition = { top: 0, left: 0 };
    this.newCommentHotspot = { display: 'none', top: 0, left: 0 };
  }
}
