export const environment = {
  production: true,
  maxFilesize: 104857600, /* 200MB */
  sketchplugin: 'https://firebasestorage.googleapis.com/v0/b/sketch-inspector-6973a.appspot.com/o/static%2FSketch%20inspector.zip?alt=media&token=85b41a0e-a519-4bed-a14c-cdd6a993df47',
  firebase: {
    apiKey: 'AIzaSyBXeIRT5PYTDlGxEn7Aotot-7Azx8387Jg',
    authDomain: 'sketch-inspector-6973a.firebaseapp.com',
    databaseURL: 'https://sketch-inspector-6973a.firebaseio.com',
    projectId: 'sketch-inspector-6973a',
    storageBucket: 'sketch-inspector-6973a.appspot.com',
    messagingSenderId: '799341255376'
  }
};
