// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  maxFilesize: 104857600, /* 100MB */
  sketchplugin: 'https://firebasestorage.googleapis.com/v0/b/sketch-inspector-6973a.appspot.com/o/static%2FSketch%20inspector.zip?alt=media&token=85b41a0e-a519-4bed-a14c-cdd6a993df47',
  firebase: {
    apiKey: 'AIzaSyBXeIRT5PYTDlGxEn7Aotot-7Azx8387Jg',
    authDomain: 'sketch-inspector-6973a.firebaseapp.com',
    databaseURL: 'https://sketch-inspector-6973a.firebaseio.com',
    projectId: 'sketch-inspector-6973a',
    storageBucket: 'sketch-inspector-6973a.appspot.com',
    messagingSenderId: '799341255376'
  }
};
